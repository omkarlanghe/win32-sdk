#include "hello.h"
#include "pch.h"
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Text;
using namespace Windows::UI;
using namespace Windows::UI::Xaml::Media;
using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml::Controls;


void MyCallbackMethod(Windows::UI::Xaml::ApplicationInitializationCallbackParams^ params)
{
	App^ app = ref new App();
}

int main(Array<String^>^ args)
{
	ApplicationInitializationCallback^ callback = ref new ApplicationInitializationCallback(MyCallbackMethod);

	Application::Start(callback);
	return 0;
}

void App::OnLaunched(Windows::ApplicationModel::Activation::LaunchActivatedEventArgs^ args)
{
	TextBlock^ textblock = ref new TextBlock();
	textblock->Text = "Hello World";
	textblock->FontFamily = ref new Windows::UI::Xaml::Media::FontFamily("SegoUI");
	textblock->FontSize = 120;
	textblock->FontStyle = Windows::UI::Text::FontStyle::Oblique;
	textblock->FontWeight = Windows::UI::Text::FontWeights::Bold;
	textblock->Foreground = ref new SolidColorBrush(Windows::UI::Colors::Gold);
	textblock->HorizontalAlignment = Windows::UI::Xaml::HorizontalAlignment::Center;
	textblock->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;

	Window::Current->Content = textblock;
	Window::Current->Activate();
}
