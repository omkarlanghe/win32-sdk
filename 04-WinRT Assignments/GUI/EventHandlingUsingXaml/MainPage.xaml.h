﻿//
// MainPage.xaml.h
// Declaration of the MainPage class.
//

#pragma once

#include "MainPage.g.h"
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Core; //Corewindow and keyevent args
using namespace Windows::UI::Xaml;

namespace EventHandlingUsingXaml
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public ref class MainPage sealed
	{
	public:
		void OnKeyDown(CoreWindow^ sender, KeyEventArgs^ args);
		void OnButtonClick(Object^ sender, RoutedEventArgs^ args);
		MainPage();

	};
}
