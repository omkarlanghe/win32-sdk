#pragma once
#include "pch.h"

using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Core; //Corewindow and keyevent args
using namespace Windows::UI::Xaml;
using namespace Platform;

ref class MyPage sealed : Page
{
private:
	TextBlock^ textblock;
public:
	MyPage();
	void OnKeyDown(CoreWindow^ sender, KeyEventArgs^ args);
	void OnButtonClick(Object^ sender, RoutedEventArgs^ args);
	//platforms contains Object, string, arrays
};
