#include "pch.h"
#include "MyPage.h"

using namespace Windows::ApplicationModel;
using namespace Windows::ApplicationModel::Activation;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Platform;
using namespace Windows::Foundation;
MyPage::MyPage()
{
	Window::Current->CoreWindow->KeyDown += ref new TypedEventHandler<CoreWindow^, KeyEventArgs^>(this,  &MyPage::OnKeyDown);
	
	Grid^ grid = ref new Grid();
	grid->Background = ref new SolidColorBrush(Windows::UI::Colors::Black);

	textblock = ref new TextBlock();
	textblock->Text = "Event Handling in WinRT";
	textblock->FontFamily = ref new Windows::UI::Xaml::Media::FontFamily("Seoge UI");
	textblock->FontSize = 120;
	textblock->FontStyle = Windows::UI::Text::FontStyle::Oblique;
	textblock->FontWeight = Windows::UI::Text::FontWeights::Bold;
	textblock->Foreground = ref new SolidColorBrush(Windows::UI::Colors::Gold);
	textblock->HorizontalAlignment = Windows::UI::Xaml::HorizontalAlignment::Center;
	textblock->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;

	grid->Children->Append(textblock);
	
	Button^ button = ref new Button();
	button->Content = "Press Me";
	button->Width = 400;
	button->Height = 200;
	button->BorderThickness = 12;
	button->BorderBrush = ref new SolidColorBrush(Windows::UI::Colors::Gold);
	button->Foreground = ref new SolidColorBrush(Windows::UI::Colors::Red);
	button->FontFamily = ref new Windows::UI::Xaml::Media::FontFamily("Seoge UI");
	button->FontSize = 50;
	button->FontWeight = Windows::UI::Text::FontWeights::Bold;;
	button->FontStyle = Windows::UI::Text::FontStyle::Oblique;
	button->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Bottom;
	button->HorizontalAlignment = Windows::UI::Xaml::HorizontalAlignment::Center;
	button->Click += ref new RoutedEventHandler(this, &MyPage::OnButtonClick);

	grid->Children->Append(button);
	this->Content = grid;
}

void MyPage::OnKeyDown(CoreWindow^ sender, KeyEventArgs^ args)
{
	textblock->Text = "Key is Pressed";

	if (args->VirtualKey == Windows::System::VirtualKey::Space)
	{
		textblock->Text = "Space is Pressed";
	}
}

void MyPage::OnButtonClick(Object^ sender, RoutedEventArgs^ args)
{
	textblock->Text = "Mouse is Clicked";
}

